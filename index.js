var soluzione = "";
var rispostaFromInputText = "";
var soluzioneInputText = "";
var nPartecipanti = 2;

$(document).ready(function () {//quando hai caricato la pagina web (html), esegui le seguenti linee di codice..

  $('#music').hide();

  //const perchè costante, è simile a "final"
  const urlParams = new URLSearchParams(window.location.search); //prendo il contenuto dell'url della barra di ricerca
  if (urlParams == "") { //se è vuoto
    $("#big-container").hide(); //nascondo la schermata di gioco (il box centrale, la musica, la barra della percentuale)
    var sfondoPaginaPrincipale = "url(./images/sfondoPrincipale.jpg)"; //sfondo iniziale
    $("body").css("background-image", sfondoPaginaPrincipale);
  } else { //mostro la schermata di gioco
    $('#footer').css("position", "relative"); //posiziono footer sotto la schermata di gioco
    $("#qrcodes").hide();//nascondo i qr code 
  }
  storia = urlParams.get("storia"); //recupero il valore del parametro "storia" del url nella barra di ricerca... puo' essere 1, 2 , 3
  
  // nascondo player video
  $("#ytvideo").hide();
  // nascondo textbox
  $("#textbox").hide();
  // nascondo la transizione temporale
  $("transizioneID").hide();
  $("#box-transizione").hide();
  // nascondo monete
  $("#nav").hide();
  // nascondo bottone stella poalre
  $(".clickOnImg").hide();
  // nascondo slider
  $("#sliderContainerPartecipanti").hide();
  // nascondo slider
  $("#sliderContainerPlayer").hide();
  //nascondo il personaggio all'inizio
  $("#personaggioGuida").hide();
  $("#inputText").hide();
  //nascondo la mappa
  $('mapid').hide();

  $.getJSON("story" + storia + ".json", function (json) { //prendo il json in base al valore della storia, e lo leggo. Il json è nella stessa directory del file js
    var urlCanzone = json.brano; //prendo il contenuto di "brano" e lo metto nella variabile "urlCanzone"
    $('#music').attr('src',urlCanzone);
    $("head").append( //aggiungo ad head il foglio di stile che è specifico per ogni storia. C'è anche un foglio di stile di default per tutto il sito web
      '<link rel="stylesheet" href="' + json.stile + '" type="text/css" />'
    );

    var i = 0; // è il numero della missione di partenza
    if (json.missioni[i].type == "transizione") {
      impostaSfondo(i, json);
      $(".container").hide();
      $("#box-transizione").show();
    }

    var app = new Vue({
      el: "#box",
      data() {
        return {
          titolo: json.titolo,
          text: "",
          personaggio: "",
          radioList: "",
          messagetransition: json.missioni[0].descrizione,
          imgcentral: "",
        };
      },
      methods: {
        // metodo avanti che si attiva al click sul bottone
        avanti: function (event) {
      
          if (i == json.missioni.length - 1) {
            window.location.href = "http://site181986.tw.cs.unibo.it";
          }
        
          //nascondo il titolo
          $("h1").hide();

          $("#personaggioGuida").show();
          $("#nav").show();
         
          if (json.missioni[i].type == "radioButtons") {
            let checked = false;
            // per ogni elemento del vettore controlla se è stato selezionato
            for (let i = 0; i < app.radioList.length; i++) {
              $("#" + i).is(":checked") ? (checked = true) : "";
            }
            if (!checked) {
              alert("Seleziona un bottone");
            } else {
              leggiMissione(++i, json, app);
            }
          } else {
            leggiMissione(++i, json, app);
          }     

          // modifica i punti
          if (json.missioni[i].punti) {
            modificaPunti(json, app, true, i);
          }

          aggiungiIconaPunti(i, json);
          setProgress(i, json);
        },

        indietro: function (event) {
          if (i > 0) {
            i--;
            impostaSfondo(i, json);

            // riattivo il pulsante invio qualora torni indietro dalla schermata del tocco per la stella polare
            $("#avantiBtn").prop("disabled", false);

            // modifica i punti
            if (json.missioni[i + 1].punti) {
              modificaPunti(json, app, false, i + 1);
            }

            if (json.missioni[i].type == "transizione") {
              $("h1").show();
              // qualora tornassi indietro mostro il box transizione e il suo contenuto
              $("#box-transizione").show();
              $("#transizioneID").show();
              $("#avantiBtnPrimo").show();
              $(".container").hide();
              $("#personaggioGuida").hide();
              $("#nav").hide();
              if(i == 0){
                $("#punti").hide();

              }
              app.messagetransition = json.missioni[i].descrizione;
            } else if (json.missioni[i].type == "rispostaRadioB") {
              leggiMissione(i, json, app);
              app.text = risposteDate.pop();
            } else {
              $(".container").show();
              leggiMissione(i, json, app);
            }
          }
          setProgress(i, json);
        },
      },
    });
  });
});
var mappa = null;
var marker = null;
var popup = null;
function costruisciMappa(latitudine, longitudine, luogo){
    latitudine = parseFloat(latitudine)
    longitudine = parseFloat(longitudine)
    if(mappa == null){
      mappa = L.map('mapid').setView([latitudine,longitudine], 13);
    }
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiYm9ubnliYXkiLCJhIjoiY2tjeXptdDZzMGV5djMycHY0YW05OWowdyJ9.ZcizI9TxiHkPb-RCdGO7IQ', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoiYm9ubnliYXkiLCJhIjoiY2tjeXptdDZzMGV5djMycHY0YW05OWowdyJ9.ZcizI9TxiHkPb-RCdGO7IQ'
    }).addTo(mappa);
     //aggiunta marker
     if (marker) {
      mappa.removeLayer(marker);
    }
    if (popup){
      mappa.removeLayer(popup)
    }
     marker = L.marker([latitudine,longitudine]).addTo(mappa);
     console.log(latitudine,longitudine)
   
     popup = marker.bindPopup('<b>'+luogo+'</b><br/> Eccoci qui.');
     popup.openPopup();
}



var puntiRicevuti = [];
var risposteDate = [];

function modificaPunti(json, app, avanti, i) {
  var puntiString = $("#tuoi_punti").text();
  var punti = parseInt(puntiString);
  if (avanti) {
    // caso scelta multipla
    if (json.missioni[i].punti.length > 1) {
      app.text = json.missioni[i].risposte[$(":checked").attr("id")];
      risposteDate.push(app.text);
      // aggiungo al vettore i punti ricevuti in caso di scelta multipla
      puntiRicevuti.push(
        parseInt(json.missioni[i].punti[$(":checked").attr("id")])
      );
      punti += puntiRicevuti[puntiRicevuti.length - 1];
    } else {
      // caso singolo
      punti += parseInt(json.missioni[i].punti[0]);
      puntiRicevuti.push(parseInt(json.missioni[i].punti[0]));
    }
  } else {
    // sottraggo al vettore i punti ricevuti in caso di scelta multipla
    punti -= puntiRicevuti.pop();
  }
  $("#tuoi_punti").text(punti);
}

// imposta la percentuale
function setProgress(i, json) {
  var percentuale = (((i + 1) / json.missioni.length) * 100).toFixed(0);
  $("#percentuale").text(percentuale + "%");
  $("#progressionBar").css("width", percentuale + "%");
}

function impostaSfondo(i, json) {
  if (json.missioni[i].hasOwnProperty("background")) {
    let urlSfondo = "url(" + json.missioni[i].background + ")";
    $("body").css("background-image", urlSfondo);
  }
}

//metodo che aggiunge l'icona dei punti
function aggiungiIconaPunti(i, json) {
  if ( i == 1) {
    $("#nav").prepend(
      "<img id='punti' src='" + json.iconaPunti + "' alt='icona dei punti' />"
    );
  }
  
}

function leggiMissione(i, json, app) {
  if(json.missioni[i-1].type == "inputText"){
    $('#inputText').val("");
  }

  //disabilito pulsante avanti inizialmente, se esiste una risposta nella missione è inputText
  if(json.missioni[i].hasOwnProperty("rispostaInputText")){
    $("#avantiBtn").prop("disabled", true);
  }
  //controllo utile alla missione 3 negli inserimenti dei risultati delle equazioni
  if(json.missioni[i].hasOwnProperty("rispostaInputText")){
    //soluzioneInputText = rispostaFromInputText;
    if(Array.isArray(json.missioni[i].rispostaInputText)){
      soluzioneInputText = json.missioni[i].rispostaInputText
      soluzioneInputText = soluzioneInputText[playerNumber-1]
    }else{
      soluzioneInputText = json.missioni[i].rispostaInputText;
    }
  }


  if(json.missioni[i].type=="inputText"){
    $("#inputText").show();
  }else{
    $("#inputText").hide();
    $("#labelInputText").hide();
  }

  if(json.missioni[i].type == "mappa"){
    $('#mapid').show();
    costruisciMappa(json.missioni[i].latitudine,json.missioni[i].longitudine,json.missioni[i].luogo);
    
  }else{
    $('#mapid').hide();
  }

  //aggiungiNaveLoading(i);
  impostaSfondo(i, json);

  app.text = json.missioni[i].descrizione;
  // se esiste il campo personaggio impostalo e fallo vedere
  //json.missioni[i].hasOwnProperty('personaggio') ? app.personaggio = json.missioni[i].personaggio : app.personaggio = "";
  if (json.missioni[i].hasOwnProperty("personaggio")) {
    $("#personaggioGuida").show();
    app.personaggio = json.missioni[i].personaggio;
  } else {
    $("#personaggioGuida").hide();
    app.personaggio = "";
  }
  // se la missione prevede dei radio buttons li inserisco
  if (json.missioni[i].type == "radioButtons") {
    // costruisco i bottoni
    app.radioList = json.missioni[i].buttons;
  } else {
    // altrimenti vengono resettati
    app.radioList = "";
  }

  if (json.missioni[i].type == "video") {
    $("#ytvideo").prop("src", json.missioni[i].url);
    $("#ytvideo").show();
  } else {
    $("#ytvideo").hide();
  }

  if (
    json.missioni[i].rispostaType == "foto" ||
    json.missioni[i].rispostaType == "video"
  ) {
    $(".container").css("width", "100%");
    $(".container").css("max-height", "100%");
    $(".container").css("margin-bottom", "50px");
    $(
      '<label id="fileLabel">Carica:&nbsp;&nbsp;</label><input type="file" id="fileInput" name="foto">'
    ).insertAfter("#ytvideo");
  } else {
    $("#ytvideo").prop("src", "");
    $(".container").css("width", "500px");
    $("#fileInput, #fileLabel").remove();
  }

  if (json.missioni[i].type == "textBox") {
    $("#textbox").show();
  } else {
    $("#textbox").hide();
  }

  // gestione delle "transizione temporanee"
  if (json.missioni[i].type == "transizione") {
    $("h1").show();
    $("#box-transizione").show();
    //nascondo la descrizione e faccio comparire il testo della transizione temporale
    $(".container").hide();
    $("#transizioneID").show();
    app.text = "";
    app.messagetransition = json.missioni[i].descrizione;
    app.titolo = "";
    $("#avantiBtnPrimo").show();
  } else {
    $("#box-transizione").hide();
    //$('.container').fadeOut('fast');
    //$('.container').fadeIn('slow');
    app.titolo = json.titolo;
    $("#transizioneID").hide();
    app.text = json.missioni[i].descrizione;
    $("#avantiBtnPrimo").hide();
    $(".container").show();
  }

  if (json.missioni[i].type == "img") {
    if(Array.isArray(json.missioni[i].immagine)){
      $("#immagine").prop("src", json.missioni[i].immagine[playerNumber-1]);
      $("#immagine").show();
    }else{
      $("#immagine").prop("src", json.missioni[i].immagine);
      $("#immagine").show();
    }
    
  
  } else {
    $("#immagine").hide();
  }

  if (json.missioni[i].type == "audio") {
    $("#audio").show();
    $("#audio").prop("src", json.missioni[i].audio);
  } else {
    $("audio").hide();
  }

  if (json.missioni[i].hasOwnProperty("img")) {
    $("#immagine").show();
  }
  if (json.missioni[i].hasOwnProperty("rispostaType")) {
    if (json.missioni[i].rispostaType == "clickImg") {
      $("#avantiBtn").prop("disabled", true);
      $(".clickOnImg").show();
      $(".clickOnImg").click(function (e) {
        e.stopImmediatePropagation();
        e.preventDefault();
        alert("Ottimo, l'hai trovata! :)");
        $("#avantiBtn").prop("disabled", false);
      });
    }
  } else {
    $(".clickOnImg").hide();
  }

  if (json.missioni[i].type == "sliderPartecipanti") {
    $("#sliderPartecipanti").attr("min", json.missioni[i].min);
    $("#sliderPartecipanti").attr("max", json.missioni[i].max);
    $("#minPartecipanti").text(json.missioni[i].min);
    $("#maxPartecipanti").text(json.missioni[i].max);
    $("#sliderContainerPartecipanti").show();
    $("#avantiBtn").prop("disabled", true);
  } else {
    //nelle missioni successive nascondo lo slider
    $("#sliderContainerPartecipanti").hide();
  }

  if (json.missioni[i].type == "sliderPlayer") {
    $("#sliderPlayer").attr("min", json.missioni[i].min);
    $("#sliderPlayer").attr("max", nPartecipanti);
    $("#minPlayer").text(json.missioni[i].min);
    $("#maxPlayer").text(nPartecipanti);
    $("#sliderContainerPlayer").show();
    $("#avantiBtn").prop("disabled", true);
  } else {
    //nelle missioni successive nascondo lo slider
    $("#sliderContainerPlayer").hide();
  }


  if (json.missioni[i].type == "soluzione") {
    soluzione = json.missioni[i].soluzione;
    //lunghezza di ogni parte di soluzione che va ad ogni giocatore (l'ultima sarà più corta)
    var chuncks = parseInt(soluzione.length / nPartecipanti) + 1;
    //assegno a splitted solution unn array dove ogni cella corrisponde alla soluzione da assegnare al giocatore n
    var splittedSolution = soluzione.match(
      new RegExp(".{1," + chuncks + "}", "g")
    );
    //assegno la soluzione n al giocatore n
    var mySolution = splittedSolution[playerNumber - 1];
    //faccio il display di mySolution, ovvero solo la parte n di soluzione che poi dovranno essere 
    //unite dai giocatori nella textarea
    $("#description").append('<p id="mySolution">' + mySolution + "</p>");
    $("#mySolution").css("font-weight", "bold");
    $("#mySolution").css("font-size", 30);
    $("#indietroBtn").prop("disabled", true);
  } else {
    $("#mySolution").remove();
    $("#indietroBtn").prop("disabled", false);
  }

  if (json.missioni[i].type == "textBoxWithCheck") {
    $("#textBoxWithCheck").val("")
    $("#textBoxWithCheck").show();
    $("#avantiBtn").prop("disabled", true);
  } else {
    $("#textBoxWithCheck").hide();
  }
}

function setPartecipanti() {
  nPartecipanti = parseInt($("#sliderPartecipanti").val());
  $("#currentValuePartecipanti").text("Valore selezionato: " + nPartecipanti);
  $("#avantiBtn").prop("disabled", false);
}

var playerNumber = 1;
function setPlayer() {
  playerNumber = parseInt($("#sliderPlayer").val());
  $("#currentValuePlayer").text("Valore selezionato: " + playerNumber);
  $("#avantiBtn").prop("disabled", false);
}

var inputText = "";
function checkTextBox() {
  inputText = $("#textBoxWithCheck").val();
  if (inputText == soluzione) {
    $("#avantiBtn").prop("disabled", false);
  } else {
    $("#avantiBtn").prop("disabled", true);
  }
}

var inText = "";
function checkInputBox() {
  inText = $("#inputText").val();
  if (inText.toLowerCase() == soluzioneInputText.toLowerCase()) {
    $("#avantiBtn").prop("disabled", false);
  } else {
    $("#avantiBtn").prop("disabled", true);
  }
}

Vue.component("radio-item", {
  props: ["radio"],
  template:
    '<div class="radioContainer custom-control custom-radio" ><input type="radio" v-bind:id="radio.id" name="radioButtons" v-bind:value="radio.id" class="custom-control-input"> <label :for="radio.id" class="custom-control-label">{{radio.text}}</label></div>',
});

Vue.component("transizione", {
  props: ["messagetransition"],
  template: '<div><h1 id="messTransition"> {{messagetransition}} </h1></div>',
});

Vue.component("immagine-centrale", {
  props: ["imgcentral"],
  template:
    '<div class="imgAndButton"><img src="" id="immagine" alt="immagineGenerica" > {{imgcentral}} <button class="clickOnImg"></button></div>',
});
