var express = require('express');
var app = express();

app.get('/', function (req, res, next) {
   console.log("Got a GET request for the homepage");
   //res.send('Hello GET');
   var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
   if (ip.substr(0, 7) == "::ffff:") {
      ip = ip.substr(7)
   }
   console.log("Indirizzo IP della richiesta: "+ip);
   next();
})

//app.use(function (req, res, next) {
  //console.log('Time:', Date.now());
  //next();
//});

app.use(express.static('./'));
var server = app.listen(8080, function () {
  var host = server.address().address
   var port = server.address().port
   console.log("Example app listening at http://%s:%s", host, port)
})
